﻿// キーの状態
var key = new Array();

// キーが押された時の処理
document.onkeydown = function (e) {
	// 押されたキーをtrueにする
	key[e.keyCode] = true;
};

// キーが離された時の処理
document.onkeyup = function (e) {
	// 離されたキーをfalseにする
	key[e.keyCode] = false;
};

var renderer = PIXI.autoDetectRenderer(innerWidth, innerHeight, { backgroundColor: 0xFFFFFF });
document.getElementById("bgmariko").appendChild(renderer.view);

var stage = new PIXI.Container();

// RigidBody
// 衝突判定のある物体にはこのクラスをプロトタイプにする

// コンストラクタ
function RigidBody(x, y, gravity, texture)
{
	// スプライト
	this.sprite = new PIXI.Sprite(texture);

	// 座標を設定
	this.sprite.position.x = x;
	this.sprite.position.y = y;

	// アンカーを設定
	this.sprite.anchor.x = 0.5;
	this.sprite.anchor.y = 0.5;

	// 速度を設定
	this.velocity = { x: 0, y: 0 };

	// 重力を設定
	this.gravity = gravity;

	// 下が物体に当たっているか
	this.bottom = false;

	// ステージに追加する
	stage.addChild(this.sprite);
}

// 幅
RigidBody.prototype.WIDTH = 32;
// 高さ
RigidBody.prototype.HEIGHT = 32;

// 衝突判定の処理
RigidBody.prototype.Collition = function ()
{
	if (this.sprite.position.x < this.WIDTH / 2)
	{
		this.sprite.position.x = this.WIDTH / 2;
		this.velocity.x = 0;
	}

	if (this.sprite.position.x > innerWidth - this.WIDTH / 2)
	{
		this.sprite.position.x = innerWidth - this.WIDTH / 2;
		this.velocity.x = 0;
	}

	if (this.sprite.position.y < this.HEIGHT / 2)
	{
		this.sprite.position.y = this.HEIGHT / 2;
		this.velocity.y = 0;
	}

	if (this.sprite.position.y > innerHeight - this.HEIGHT / 2)
	{
		this.sprite.position.y = innerHeight - this.HEIGHT / 2;
		this.velocity.y = 0;
		this.bottom = true;
	}
	else
	{
		this.bottom = false;
	}
}

// 重力を加える
RigidBody.prototype.Gravity = function ()
{
	this.velocity.y += this.gravity;
	this.sprite.position.y += this.velocity.y;
}

// 摩擦を加える
RigidBody.prototype.Friction = function ()
{
	if (this.bottom)
	{
		this.velocity.x *= 0.75;
	}

	this.sprite.position.x += this.velocity.x;
}

// Mario
// プレイヤー

// コンストラクタ
function Mario()
{
	RigidBody.apply(this, arguments);
}

// RigidBodyを継承する
Mario.prototype = new RigidBody(100, 0, 1, PIXI.Texture.fromImage("games/bgmariko/images/mario.gif"));

// 動く
Mario.prototype.Move = function ()
{
	if (key[90])
	{
		Mario.prototype.velocity.y -= 2;
		Mario.prototype.sprite.position.y -= 1;
	}

	if (key[37])
	{
		Mario.prototype.velocity.x -= 1;
		Mario.prototype.sprite.scale.x = -1;
	}

	if (key[39])
	{
		Mario.prototype.velocity.x += 1;
		Mario.prototype.sprite.scale.x = 1;
	}
}

// 座標を更新する
Mario.prototype.Update = function ()
{
	// 重力を加える
	Mario.prototype.Gravity();

	// 摩擦を加える
	Mario.prototype.Friction();

	// 動かす
	this.Move();

	// 衝突判定
	Mario.prototype.Collition();
}

var mario = new Mario();

Main();

function Main()
{
	requestAnimationFrame(Main);
	
	// 座標を更新する
	mario.Update();

	renderer.render(stage);
}
